docker pull neo4j

docker run \
    -p 7474:7474 -p 7687:7687 \
    --volume=$PWD/neo4j_data:/data \
    --name jota/neo4j \
    neo4j

port 7474 for http browser access
port 7687 for db access

login: neo4j
new password: enter
