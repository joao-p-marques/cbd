from neomodel import StructuredNode, StructuredRel, UniqueIdProperty, StringProperty, IntegerProperty, DateTimeProperty, RelationshipTo,RelationshipFrom, config,  ZeroOrMore
import neo4j
import datetime

# TODO: change this 
USERNAME = 'neo4j'
PASSWORD = 'enter'

config.DATABASE_URL = f'bolt://{USERNAME}:{PASSWORD}@localhost:7687'

class Rides(StructuredRel):
    duration = IntegerProperty()
    start_date = DateTimeProperty(
            default = lambda: datetime.datetime.now()
    )
    end_date = DateTimeProperty(
            default = lambda: datetime.datetime.now()
    )
    start_station_number = IntegerProperty()
    start_station = StringProperty()
    end_station_number = IntegerProperty()
    end_station = StringProperty()

class Bike(StructuredNode):
    bike_number = UniqueIdProperty()
    bike_user = RelationshipFrom('BikeUser', 'RIDES', model=Rides, cardinality=ZeroOrMore)

class BikeUser(StructuredNode):
    user_type = StringProperty()
    bike = RelationshipTo('Bike', 'RIDES', model=Rides, cardinality=ZeroOrMore)

# def load_csv():
#     FILE_NAME = '2016 Q1.csv'

#     query = f"LOAD CSV WITH HEADERS FROM 'file:///{FILE_NAME}' AS line"
#     query += " MERGE (bike:Bike { bike_number : line.Bike_number })"
#     query += " MERGE (user:User { user_type : line.Member_Type })"
#     query += " CREATE (user)-[:RIDES duration : "

def load_csv(file_name):
    with open(file_name, 'r') as f:
        for i, line in enumerate(f):
            if i == 0:
                continue
            if i > 500:
                break

            elems = line.rstrip().split(',')

            Duration = elems[0]
            Start_date = datetime.datetime.strptime(elems[1], '%m/%d/%Y %H:%M')
            End_date = datetime.datetime.strptime(elems[2], '%m/%d/%Y %H:%M')
            Start_station_number = elems[3]
            Start_station = elems[4]
            End_station_number = elems[5]
            End_station = elems[6]
            Bike_number = elems[7]
            Member_Type = elems[8]

            u = BikeUser(user_type=Member_Type).save()

            b = Bike.nodes.get_or_none(bike_number=Bike_number)
            if b is None:
                b = Bike(bike_number=Bike_number).save()

            rel = u.bike.connect(b, 
                    {
                        'duration' : Duration,
                        'start_date' : Start_date,
                        'end_date' : End_date,
                        'start_station_number' : Start_station_number,
                        'start_station' : Start_station,
                        'end_station_number' : End_station_number,
                        'end_station' : End_station,
                    }
                )

for n in Bike.nodes.all():
    n.delete()
print('Deleted all bikes..')
for n in BikeUser.nodes.all():
    n.delete()
print('Deleted all users...')

load_csv('2016 Q1.csv')
print('Loaded.')

# print(Bike.nodes.all())

# get all the bikes used more than once
bikes_used_more_than_once = []
for bike in Bike.nodes.all():
    # print(bike.bike_user.all())
    if len(bike.bike_user.all()) > 1:
        bikes_used_more_than_once.append(bike)

print(len(bikes_used_more_than_once))

# get all the bikes that came from 31280 station
bikes = []
for bike in Bike.nodes.all():
    for user in bike.bike_user.all():
        rels = bike.bike_user.all_relationships(user)
        for rel in rels:
            # print(rel.start_station_number)
            if str(rel.start_station_number) == '31280':
                bikes.append(bike)

# print(bikes)
print(len(bikes))

# get all the user who used more than one bike
users = []
for user in BikeUser.nodes.all():
    # print(user.bike.all())
    if len(user.bike.all()) > 1:
        users.append(user)

print(len(users))

# get all the bikes ridden on 3/31/2016
bikes = []
date = datetime.datetime.strptime('3/31/2016', '%m/%d/%Y')
for bike in Bike.nodes.all():
    for user in bike.bike_user.all():
        rels = bike.bike_user.all_relationships(user)
        for rel in rels:
            if date.date() == rel.start_date.date():
                bikes.append(bike)

# print(bikes)
print(len(bikes))

# get all the bikes ridden (end_date) on 4/1/2016
bikes = []
date = datetime.datetime.strptime('4/1/2016', '%m/%d/%Y')
for bike in Bike.nodes.all():
    for user in bike.bike_user.all():
        rels = bike.bike_user.all_relationships(user)
        for rel in rels:
            if date.date() == rel.end_date.date():
                bikes.append(bike)

# print(bikes)
print(len(bikes))

# get all the bikes that came from New Hampshire Ave
bikes = []
for bike in Bike.nodes.all():
    for user in bike.bike_user.all():
        rels = bike.bike_user.all_relationships(user)
        for rel in rels:
            if 'New Hampshire Ave' in str(rel.start_station):
                bikes.append(bike)

# print(bikes)
print(len(bikes))

# get all the bikes that arrived at Rhode Island Ave
bikes = []
for bike in Bike.nodes.all():
    for user in bike.bike_user.all():
        rels = bike.bike_user.all_relationships(user)
        for rel in rels:
            if 'Rhode Island Ave' in str(rel.end_station):
                bikes.append(bike)

# print(bikes)
print(len(bikes))

# get all the Registered users
users = BikeUser.nodes.filter(user_type__exact='Registered')
print(len(users))

# get all the Casual users
users = BikeUser.nodes.filter(user_type__exact='Casual')
print(len(users))

# get all the bikes ridden by Casual users
bikes = []
for user in BikeUser.nodes.all():
    if user.user_type == 'Casual':
        for bike in user.bike.all():
            bikes.append(bike)

print(len(bikes))
