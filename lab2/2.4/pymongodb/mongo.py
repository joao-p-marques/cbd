
import pymongo
import time

class Mongo():
    def __init__(self):
        self.client = pymongo.MongoClient("mongodb://localhost/restaurants?retryWrites=true&w=majority")
        self.db = self.client.restaurants

    def insert_document(self, doc):
        self.db.rest.insert_one(doc)

    def update_document(self, doc_id, doc):
        self.db.rest.update_one(
                { '_id' : doc_id },
                { '$set' : doc },
                upsert=False
                )

    def search_document(self, query, projection=None):
        res = None
        if projection is None:
            res = self.db.rest.find(query, {})
        else:
            res = self.db.rest.find(query, projection)
        return [o for o in res]

    def add_index(self, field, ascending=True):
        if ascending:
            self.db.rest.create_index(field)
        else:
            self.db.rest.create_index((field, pymongo.DESCENDING))

    def count_rest_by_localidades(self):
        l = self.db.rest.aggregate(
                [
                    {'$group' : {'_id' : '$localidade', 'count' : {'$sum' : 1}}}
                ]
            )
        return [o for o in l]

    def count_localidades(self):
        return len(self.count_rest_by_localidades())

    def count_rest_by_localidades_by_gastronomia(self):
        l = self.db.rest.aggregate(
                [
                    {'$group' : {'_id' : { 'localidade' : '$localidade', 'gastronomia' : '$gastronomia'}, 'count' : {'$sum':1}}}
                ]
            )
        return [o for o in l]

    def get_rest_with_name_closer_to(self, name):
        l = self.db.rest.find(
                {
                    'nome' : { '$regex' : ".*" + name + ".*" } 
                }
            )
        return [o for o in l]





mongo = Mongo()
print(mongo.db)
print()

# print(mongo.search_document({}))

mongo.db.rest.drop_indexes()

start_time = time.time()
mongo.search_document({'gastronomia':'American'})
print("BEFORE INDEXES (1):", time.time() - start_time)

start_time = time.time()
mongo.search_document({'localidade':'Manhattan'})
print("BEFORE INDEXES (2):", time.time() - start_time)

mongo.add_index('localidade')
mongo.add_index('gastronomia')
mongo.add_index('nome')

start_time = time.time()
mongo.search_document({'gastronomia':'American'})
print("AFTER INDEXES (1):", time.time() - start_time)

start_time = time.time()
mongo.search_document({'localidade':'Manhattan'})
print("AFTER INDEXES (2):", time.time() - start_time)

print()

print('Numero de localidades distintas:', mongo.count_localidades())

print('Numero de restaurantes por localidade:')
for l in mongo.count_rest_by_localidades():
    print(' ->', l['_id'], '-', l['count'])

print('Numero de restaurantes por localidade e gastronomia:')
for l in mongo.count_rest_by_localidades_by_gastronomia():
    print(' ->', l['_id']['localidade'], '|', l['_id']['gastronomia'], '-', l['count'])

name = 'Park'
print(f'Nome de restaurantes contendo {name} no nome:')
for l in mongo.get_rest_with_name_closer_to(name):
    print(' ->', l['nome'])
