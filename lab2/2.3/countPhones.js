
countPhones = function (country) {

  db.phones.aggregate(
    [
      { $match :
        {
          "components.country" : country
        }
      },
      { $group : 
        {
          _id : "$components.prefix",
          num_phones : { $sum : 1 }
        }
      },
      { $sort : 
          { num_phones : 1 }
      }
    ]
  ).forEach(printjson);
  // ).toArray(function(err, result) {
  //   if (err) throw err;
  //   console.log(result);
  //   db.close()
  // });

}
