
findPattern = function (number_obj) {
  number = number_obj.components.number;

  capicua = true
  seq = true
  non_rep = true

  for(var i=0; i < number.length; i++){
    if (i < number.length/2) {
      if (number.charAt(i) != number.charAt(number.length-i)){
        capicua = false
      }
    }
    if (i < number.length-1) {
      if (number.charAt(i)+1 != number.charAt(i+1)) {
        seq = false
      }
    }
    if (number.substr(0, i).lastIndexOf(number.charAt(i)) != -1) {
      non_rep = false
    }

    if (capicua)
      print(number + " -> capicua")
    if (seq)
      print(number + " -> seq")
    if (non_rep)
      print(number + " -> non_rep")
  }
}

findPatternPhones = function (country) {

  db.phones.find(
    {
      "components.country" : country
    }
  ).forEach(findPattern);

}
