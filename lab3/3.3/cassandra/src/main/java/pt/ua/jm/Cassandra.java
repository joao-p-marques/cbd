
package pt.ua.jm;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import java.util.*;

public class Cassandra {

  Cluster cluster;
  Session session;

  public Cassandra(String hostname, String keyspace) {
    this.setCluster(hostname);
    this.setSession(keyspace);
  }

  public void setCluster(String hostname) {
    //Creating Cluster object
    this.cluster = Cluster.builder().addContactPoint("127.0.0.1").build();
  }

  public void setSession(String keyspace) {
    //Creating Session object
    Session session = cluster.connect(keyspace);
  }

  public void add(String table, String[] columns, String[] args) {
    if (columns.length != args.length)
      System.out.println("Invalid column names and args");

    StringBuilder query_builder = new StringBuilder();
    query_builder.append("INSERT INTO " + table + " (");
    query_builder.append(columns[0]);
    for(int i=1; i<columns.length; i++)
      query_builder.append(", " + columns[i]);
    query_builder.append(") ");

    query_builder.append(" VALUES (");
    query_builder.append(args[0]);
    for(int i=1; i<args.length; i++)
      query_builder.append(", " + args[i]);
    query_builder.append("); ");

    String query = query_builder.toString();
    System.out.println("Query: " + query);
    this.session.execute(query);
  }

}
