package pt.ua.jm;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        Cassandra cassandra = new Cassandra("localhost", "movies");

        // INSERT INTO users (username, name, email) VALUES ('uncingbawd', 'uncingbawd', 'uncingbawd@mail.com');
        String[] columns = { "username", "name", "email" };
        String[] data = { "uncingbawd2", "uncingbawd2", "uncingbawd2@gmail.com" };
        cassandra.add("users", columns, data);
    }
}
