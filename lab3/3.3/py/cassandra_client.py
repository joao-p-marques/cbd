from cassandra.cluster import Cluster

cluster = Cluster(['192.168.0.1', '192.168.0.2'])

class Cassandra:
    
    def __init__(self, hostname, keyspace=None):
        self.cluster = Cluster([hostname])
        self.keyspace = None
        if keyspace is not None:
            self.connect_to_keyspace(keyspace)

    def connect_to_keyspace(self, keyspace):
        self.session = self.cluster.connect(keyspace)

    def add(self, table_name, fiels, args):
        if len(fiels) != len(args):
            print('Invalid column names and args!')

        query = f'INSERT INTO {table_name} ('
        for field in fiels:
            query += f'{field}'
            if field != fiels[-1]:
                query += ', '

        query += ') VALUES ('
        for arg in args:
            query += f'\'{arg}\''
            if arg != args[-1]:
                query += ', '
        query += ');'

        print(f'Executing query: {query}')
        self.session.execute(query)

    def update(self, table_name, fields, args, filter_field, filter_arg):
        # UPDATE test_table_person
        # SET name='JoaoJoao', email='joao@mail.com'
        # WHERE id=1
        # ;
        
        if len(fields) != len(args):
            print('Invalid column names and args!')

        query = f'UPDATE {table_name} SET '
        for field, arg in zip(fields, args):
            query += f'{field}=\'{arg}\''
            if field != fields[-1]:
                query += ', '
        
        query += f' WHERE {filter_field}=\'{filter_arg}\';'

        print(f'Executing query: {query}')
        self.session.execute(query)

    def remove(self, table_name, filter_field, filter_arg):
        # DELETE [<COLUMN NAME>]
        # FROM test_table_person
        # WHERE name='Joao2'
        # ;

        query = f'DELETE FROM {table_name} WHERE {filter_field}=\'{filter_arg}\';'

        print(f'Executing query: {query}')
        self.session.execute(query)

    def read(self, table_name, fields='*', filter_field=None, filter_arg=None, options=None, allow_filtering=False):
        # SELECT FROM test_table_person
        # WHERE name='Joao2'
        # ALLOW FILTERING
        # ;

        if (filter_field is not None and filter_arg is None or
                filter_field is None and filter_arg is not None):
            print('Invalid filtering!')

        query = f'SELECT {fields} FROM {table_name}'

        if filter_field:
            query += f' WHERE {filter_field}=\'{filter_arg}\''

        if options is not None:
            query += f' {options}'

        if allow_filtering:
            query += ' ALLOW FILTERING'

        query += ';'
        print(f'Executing query: {query}')
        rows = self.session.execute(query)

        for row in rows:
            print(row)
