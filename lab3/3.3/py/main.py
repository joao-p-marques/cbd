
from cassandra_client import *

cassandra = Cassandra('localhost', 'movies')

cassandra.add('users', [ "username", "name", "email" ], [ "uncingbawd2", "uncingbawd2", "uncingbawd2@gmail.com" ])
cassandra.update('users', [ "name", "email" ], [ "uncingbawd3", "uncingbawd3@gmail.com" ], 'username', 'uncingbawd2')
cassandra.read('users', fields='*', filter_field='username', filter_arg='uncingbawd2')
cassandra.remove('users', filter_field='username', filter_arg='uncingbawd2')
cassandra.read('users', fields='*')

# select video_name, avg(rating) as avg_rating, count(*) as num_rating from user_video_rating where video_name='Movie1';
cassandra.read('user_video_rating', fields='video_name, avg(rating) as avg_rating, count(*) as num_rating', filter_field='video_name', filter_arg='Movie1')

# select * from comments where video_name='Movie3' limit 3;
cassandra.read('comments', fields='*', filter_field='video_name', filter_arg='Movie3', options='limit 3')

# select name, tags from videos where name='Movie1';
cassandra.read('videos', fields='name, tags', filter_field='name', filter_arg='Movie1')
