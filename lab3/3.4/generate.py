
import random

username_list = [
    'uncingbawd',
    'monasterysave',
    'fonttyler',
    'breamishcarrion',
    'evremondelucero',
    'helenaproject',
    'pumicepajamas',
    'tantalumsairey',
    'snookerrosti'
    'popinjayamelio',
    'detonatorcolossal',
    'defeatedringed',
    'pretzelswanny',
    'deservinglevitate',
    'spashedunseeing',
    'lavaranging',
    'championfilibuster',
    'gapingdad',
    'challengeinsurance',
    'cuddlealkali',
    'wingedmahjong',
    'pidgerdolerite'
    'backbonedrhyme',
    'egyptianfiddley',
    'warwickshireshadiness',
    'burnsexam',
    'electiondressage',
    'premisescountry'
    'cleftroad',
    'overtlyelf'
]

songs_tags_list = [
    'drama',
    'romance',
    'thriller',
    'comedy',
    'history',
    'action',
    'fiction'
]

query_list = []
query_list.append('use songs;')
query_list.append('\n')

## USERS
for username in username_list:
    name = username
    email = f"{username}@mail.com"
    q = f"INSERT INTO users (username, name, email, register_time) VALUES ('{username}', '{name}', '{email}', toTimestamp(now()));"
    query_list.append(q)

query_list.append('\n')

## SONGS
for i in range(30):
    song_name = 'Song' + str(i)
    artist = random.choice(username_list)
    tags = list(set(random.choices(songs_tags_list, k=3))) # select 3 elements from tags
    duration = random.randint(1, 1000)
    upload_time = 'toTimestamp(now())'
    followers = list(set(random.choices(username_list, k=5))) # select 5 random followers
    ratings = list(set(random.choices(range(1, 11), k=5))) # select 5 random ratings from 1-10
   
    q = f"INSERT INTO songs (name, artist, tags, duration, upload_time, followers, ratings) VALUES ('{song_name}', '{artist}', "

    q += "{"
    for t in tags:
        q += f"'{t}'"
        if not t == tags[-1]:
            q += ', '
    q += "}, "

    q += f'{duration}, {upload_time}, ' 

    q += "{"
    for f in followers:
        q += f"'{f}'"
        if not f == followers[-1]:
            q += ', '
    q += "}, "

    q += "["
    for r in ratings:
        q += f"{r}"
        if not r == ratings[-1]:
            q += ', '
    q += "]"

    q += ");"
    query_list.append(q)

    q2 = q.replace('songs', 'songs_by_artist')
    query_list.append(q2)

query_list.append('\n')

## COMMENTS
for i in range(50):
    user_name = random.choice(username_list)
    song_name = "Song" + str(random.choice(range(30)))
    comment = "..."
   
    q = f"INSERT INTO comments (user_name, song_name, comment, comment_time) VALUES ('{user_name}', '{song_name}', '{comment}', toTimestamp(now()));"
    query_list.append(q)

    q2 = q.replace('comments', 'comments_by_author')
    query_list.append(q2)

query_list.append('\n')

## SONG RATING
for i in range(50):
    user_name = random.choice(username_list)
    song_name = "Song" + str(random.choice(range(30)))
    rating = random.randint(1, 10)
   
    q = f"INSERT INTO song_rating_by_user (user_name, song_name, rating) VALUES ('{user_name}', '{song_name}', {rating});"
    query_list.append(q)

    q2 = q.replace('song_rating_by_user', 'song_rating_by_rating')
    query_list.append(q2)

f = open('insert.sql', 'w')
for q in query_list:
    f.write(q)
    f.write('\n')
f.close()

