
CREATE KEYSPACE "songs"
WITH replication = {'class':'SimpleStrategy', 'replication_factor' : 1};

USE songs;

CREATE TABLE users (
username text,
name text,
email text,
register_time timestamp,
PRIMARY KEY (username)
);

CREATE TABLE songs (
name text,
artist text,
tags set<text>,
duration int,
upload_time timestamp,
followers set<text>,
ratings list<int>,
PRIMARY KEY(name)
);

CREATE TABLE songs_by_name (
name text,
artist text,
tags set<text>,
duration int,
upload_time timestamp,
followers set<text>,
ratings list<int>,
PRIMARY KEY(name, upload_time)
) with CLUSTERING ORDER BY (upload_time DESC);

CREATE TABLE songs_by_artist (
name text,
artist text,
tags set<text>,
duration int,
upload_time timestamp,
followers set<text>,
ratings list<int>,
PRIMARY KEY(artist, upload_time)
) with CLUSTERING ORDER BY (upload_time DESC);

CREATE TABLE comments (
user_name text,
song_name text,
comment text,
comment_time timestamp,
PRIMARY KEY (user_name, song_name)
);

CREATE TABLE comments_by_author (
user_name text,
song_name text,
comment text,
comment_time timestamp,
PRIMARY KEY (user_name, comment_time)
) WITH CLUSTERING ORDER BY (comment_time DESC);

CREATE TABLE song_rating_by_user (
user_name text,
song_name text,
rating int,
PRIMARY KEY (user_name, song_name)
);

CREATE TABLE song_rating_by_rating (
user_name text,
song_name text,
rating int,
PRIMARY KEY (song_name, rating)
)
WITH CLUSTERING ORDER BY (rating DESC);

CREATE INDEX ON songs (name);

CREATE INDEX ON songs (artist);

create index on comments (song_name);
