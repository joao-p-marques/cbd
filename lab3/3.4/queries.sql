
use songs;

// adicionar 'someuser' como follower de 'Song1'
update songs SET followers = followers + {'someuser'} where name = 'Song1';

// mudar o email do user 'uncingbawd'
update users SET email = 'uncingbawd.uncingbawd@mail.com' where username = 'uncingbawd';

// change comment on 'Song1'
update comments SET comment='very good' where song_name='Song1';

// adicionar rating a 'Song1'
update songs SET ratings = ratings + [10] where name = 'Song1';

// mudar a duração da 'Song1'
update songs SET duration = 300 where name = 'Song1';


// remover 'Song2'
delete from songs where name = 'Song2';

// remover comentários de 'cuddlealkali' para 'Song18'
delete from comments where song_name='Song26' if user_name='wingedmahjong';

// remover comentários de 'cuddlealkali' para 'Song18'
delete from comments where song_name='Song16' if user_name='snookerrostipopinjayamelio';

INSERT INTO comments (user_name, song_name, comment, comment_time) VALUES ('pretzelswanny2', 'Song29', '...', toTimestamp(now()));
INSERT INTO comments (user_name, song_name, comment, comment_time) VALUES ('pretzelswanny2', 'Song30', '...', toTimestamp(now()));
INSERT INTO comments (user_name, song_name, comment, comment_time) VALUES ('pretzelswanny', 'Song29', '...', toTimestamp(now()));

// remover comentários de 'pretzelswanny2' para 'Song29'
delete from comments where user_name='pretzelswanny2' and song_name='Song29';


// todos os utilizadores que tiverem avaliado a Song1
select * from song_rating_by_user where song_name='Song1';

// todos os comentarios de 'cuddlealkali' na 'Song18'
select * from comments where song_name='Song26' and user_name='wingedmahjong';

// todos os coments de 'snookerrostipopinjayamelio'
select * from comments where user_name='snookerrostipopinjayamelio';

// todos os coments de 'pretzelswanny2'
select * from comments where user_name='pretzelswanny2';

// todos os coments de 'Song29'
select * from comments where song_name='Song29';


// selecionar os ratings ordenados pelo user
select * from song_rating_by_user;

// (os 5 primeiros)
select * from song_rating_by_user limit 5;

// listar todas as musicas do artist
select * from songs_by_artist where artist='uncingbawd';

// todos os comentarios de 'fonttyler' ordenados pelo time do comentário
select * from comments_by_author where user_name='fonttyler' order by song_name;