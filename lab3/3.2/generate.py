
import random

username_list = [
    'uncingbawd',
    'monasterysave',
    'fonttyler',
    'breamishcarrion',
    'evremondelucero',
    'helenaproject',
    'pumicepajamas',
    'tantalumsairey',
    'snookerrosti'
    'popinjayamelio',
    'detonatorcolossal',
    'defeatedringed',
    'pretzelswanny',
    'deservinglevitate',
    'spashedunseeing',
    'lavaranging',
    'championfilibuster',
    'gapingdad',
    'challengeinsurance',
    'cuddlealkali',
    'wingedmahjong',
    'pidgerdolerite'
    'backbonedrhyme',
    'egyptianfiddley',
    'warwickshireshadiness',
    'burnsexam',
    'electiondressage',
    'premisescountry'
    'cleftroad',
    'overtlyelf'
]

movie_tags_list = [
    'drama',
    'romance',
    'thriller',
    'comedy',
    'history',
    'action',
    'fiction'
]

query_list = []
query_list.append('use movies;')
query_list.append('\n')

## USERS
for username in username_list:
    name = username
    email = f"{username}@mail.com"
    q = f"INSERT INTO users (username, name, email, register_time) VALUES ('{username}', '{name}', '{email}');"
    query_list.append(q)

query_list.append('\n')

## VIDEOS
for i in range(30):
    movie_name = 'Movie' + str(i)
    shared_by = random.choice(username_list)
    tags = list(set(random.choices(movie_tags_list, k=3))) # select 3 elements from tags
    followers = list(set(random.choices(username_list, k=5))) # select 5 random followers
    ratings = list(set(random.choices(range(1, 11), k=5))) # select 5 random ratings from 1-10
   
    q = f"INSERT INTO videos (name, shared_by, tags, followers, ratings) VALUES ('{movie_name}', '{shared_by}', "

    q += "{"
    for t in tags:
        q += f"'{t}'"
        if not t == tags[-1]:
            q += ', '
    q += "}, "

    q += "{"
    for f in followers:
        q += f"'{f}'"
        if not f == followers[-1]:
            q += ', '
    q += "}, "

    q += "["
    for r in ratings:
        q += f"{r}"
        if not r == ratings[-1]:
            q += ', '
    q += "]"

    q += ");"
    query_list.append(q)

query_list.append('\n')

## COMMENTS
for i in range(50):
    user_uname = random.choice(username_list)
    video_name = "Movie" + str(random.choice(range(30)))
    comment = "..."
   
    q = f"INSERT INTO comments (user_uname, video_name, comment, comment_time) VALUES ('{user_uname}', '{video_name}', '{comment}', toTimestamp(now()));"
    query_list.append(q)

query_list.append('\n')

## USER VIDEO ACTION
for i in range(50):
    user_uname = random.choice(username_list)
    action_name = random.choice(['play', 'pause', 'stop'])
    video_name = "Movie" + str(random.choice(range(30)))
    video_time = random.randint(1, 600)
   
    q = f"INSERT INTO user_video_action (user_uname, action_name, video_name, video_time) VALUES ('{user_uname}', '{action_name}', '{video_name}', '{video_time}');"
    query_list.append(q)

## USER VIDEO RATING
for i in range(50):
    user_uname = random.choice(username_list)
    video_name = "Movie" + str(random.choice(range(30)))
    ratings = random.choices(range(1, 11))
   
    q = f"INSERT INTO user_video_rating (user_uname, video_name, rating) VALUES ('{user_uname}', '{video_name}', '{rating}');"
    query_list.append(q)

f = open('insert.sql', 'w')
for q in query_list:
    f.write(q)
    f.write('\n')
f.close()

