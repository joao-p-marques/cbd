
use movies;

// pesquisa de todos os vídeos de determinado autor
select * from videos where shared_by ='cuddlealkali';

// pesquisa de comentários por utilizador, ordenado inversamente pela data
select * from comments where user_uname='fonttyler';

// pesquisa de comentários por vídeos, ordenado inversamente pela data
select * from comments where video_name='Movie26';

// pesquisa do rating médio de um vídeo e quantas vezes foi votado
select video_name, avg(rating) as avg_rating, count(*) as num_rating from user_video_rating where video_name='Movie1';

// últimos 3 comentários introduzidos para um vídeo
select * from comments where video_name='Movie3' limit 3;

// Lista das tags de determinado vídeo
select name, tags from videos where name='Movie1';

// Todos os vídeos com a tag Aveiro
select name, tags from videos where tags contains 'comedy' ALLOW FILTERING;
// precisei de usar ALLOW FILTERING devido à maneira como construí a tabela.
-- Se tivesse criado outra tabela para mapear as tags para cada vídeo não seria necessário

// últimos 5 eventos de determinado vídeo realizados por um utilizador
select * from user_video_action where user_uname='challengeinsurance' allow filtering;

// Vídeos partilhados por determinado utilizador (maria1987, por exemplo) num
-- determinado período de tempo (Agosto de 2017, por exemplo);
select * from videos where shared_by='cuddlealkali' and upload_time>=toTimestamp('2019-11-1') and upload_time<=toTimestamp('2019-11-30') allow filtering;

// últimos 10 vídeos, ordenado inversamente pela data da partilhada
select * from videos_shared_by limit 10;

// Todos os seguidores (followers) de determinado vídeo
select name, followers from videos;

// Todos os comentários (dos vídeos) que determinado utilizador está a seguir (following)

// Os 5 vídeos com maior rating
select * from video_rating;

// Todos os vídeos e que mostre claramente a forma pela qual estão ordenados
select * from videos_shared_by;

// Lista com as Tags existentes e o número de vídeos catalogados com cada uma delas

