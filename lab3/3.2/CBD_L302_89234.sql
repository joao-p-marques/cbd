
CREATE KEYSPACE "movies"
WITH replication = {'class':'SimpleStrategy', 'replication_factor' : 1};

USE movies;

CREATE TABLE users (
username text,
name text,
email text,
register_time timestamp,
PRIMARY KEY (username)
);

CREATE TABLE videos (
name text,
shared_by text,
tags set<text>,
upload_time timestamp,
followers set<text>,
ratings list<int>,
PRIMARY KEY(name, upload_time)
) with CLUSTERING ORDER BY (upload_time DESC);

CREATE TABLE videos_shared_by (
name text,
shared_by text,
tags set<text>,
upload_time timestamp,
followers set<text>,
ratings list<int>,
PRIMARY KEY (shared_by, upload_time)
) with CLUSTERING ORDER BY (upload_time DESC);

CREATE TABLE videos_by_time (
name text,
shared_by text,
tags set<text>,
upload_time timestamp,
followers set<text>,
ratings list<int>,
PRIMARY KEY (name, upload_time)
) with CLUSTERING ORDER BY (upload_time DESC);

CREATE TABLE comments (
user_uname text,
video_name text,
comment text,
comment_time timestamp,
PRIMARY KEY (video_name, comment_time)
) WITH CLUSTERING ORDER BY (comment_time DESC);

CREATE TABLE comments_by_author (
user_uname text,
video_name text,
comment text,
comment_time timestamp,
PRIMARY KEY (user_uname, comment_time)
) WITH CLUSTERING ORDER BY (comment_time DESC);

CREATE TABLE user_video_action (
user_uname text,
action_name text,
video_name text,
video_time int,
action_time timestamp,
PRIMARY KEY ((user_uname, action_name))
);

CREATE TABLE user_video_rating (
    user_uname text,
    video_name text,
    rating int,
    PRIMARY KEY (video_name, user_uname)
);

CREATE TABLE video_rating (
    user_uname text,
    video_name text,
    rating int,
    PRIMARY KEY (video_name, rating)
)
WITH CLUSTERING ORDER BY (rating DESC);

CREATE INDEX ON videos (shared_by);

create index on comments (user_uname);

create index on comments (video_name);