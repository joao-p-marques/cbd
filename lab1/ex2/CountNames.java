import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Scanner;

public class CountNames {
  public static void main(String[] args) throws FileNotFoundException {
    Scanner in = new Scanner(new File("female-names.txt"));
    HashMap<Character, Integer> initials_count = new HashMap<>();

    while(in.hasNextLine()) {
      String word = in.nextLine();
      char initial = Character.toUpperCase(word.charAt(0));
      // System.out.println("Reading '" + initial + "'");
      if(initials_count.containsKey(initial)) {
        int temp = initials_count.get(initial);
        initials_count.put(initial, temp+1);
      }
      else
        initials_count.put(initial, 1);
    }
    in.close();

    System.out.println(initials_count.toString());
    PrintWriter out = new PrintWriter("initial4redis.txt");    
    PrintWriter out_redis = new PrintWriter("redis_commands.txt");    
    for(Character c : initials_count.keySet()) {
      out.println(c + " " + initials_count.get(c));
      out_redis.println("SET " + c + " " + initials_count.get(c));
    }
    out.close();
    out_redis.close();
  }
}
