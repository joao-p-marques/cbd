
package pt.ua.jm.cbd.autocomplete;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import redis.clients.jedis.Jedis;

class AutoComplete {

    Jedis jedis;

    public AutoComplete() {
        this.jedis = new Jedis("localhost");
    }

    public void insert_names() {
        if(this.jedis.dbSize() != 0) {
            System.out.println("Databasa already with " + this.jedis.dbSize() + " items.");
            return;
        }

        Scanner in;
        try {
            in = new Scanner(new File("../female-names.txt"));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            in = null;
            e.printStackTrace();
        }
        List<String> names_list = new ArrayList<>();
        
        while(in.hasNextLine()) {
            String name = in.nextLine();
            names_list.add(name);

            this.jedis.set(name, "--"); // add key to redis database with empy value
        }

        System.out.println("Insertion complete. " + this.jedis.dbSize() + " items in database.");
        in.close();
    }

    public HashSet<String> get_name_autocomplete(String str) {
        return (HashSet<String>) this.jedis.keys(str + "*");
    }

    public void get_name_from_user() {
        Scanner in = new Scanner(System.in);

        while(true) {
            System.out.print("Search for ('Enter' for quit):");
            String input = in.nextLine();

            if(input.equals(""))
                break;

            HashSet<String> res = get_name_autocomplete(input);

            for(String name : res) 
                System.out.println(name);
        }

        in.close();
    }
}