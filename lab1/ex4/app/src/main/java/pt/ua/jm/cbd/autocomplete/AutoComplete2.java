
package pt.ua.jm.cbd.autocomplete;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import redis.clients.jedis.Jedis;

class AutoComplete2 {

    Jedis jedis;

    public AutoComplete2() {
        this.jedis = new Jedis("localhost");
    }

    public void insert_names() {
        if(this.jedis.dbSize() != 0) {
            System.out.println("Databasa already with " + this.jedis.dbSize() + " items.");
            return;
        }

        Scanner in;
        try {
            in = new Scanner(new File("../nomes-registados-2018.csv"));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            in = null;
            e.printStackTrace();
        }
       
        int i = 0;
        while(in.hasNextLine()) {
            String input = in.nextLine();
            String name = input.split(",")[0];
            // Double count = Double.parseDouble(input.split(";")[2]);
            String count = input.split(",")[2];

            this.jedis.set("Person:" + i + ":" + name, count); // add key to redis database with empy value
            // this.jedis.zadd("Persons:namecount", count, "Person:" + i);

            i++;
        }

        System.out.println("Insertion complete. " + this.jedis.dbSize() + " items in database.");
        in.close();
    }

    //pretty print a map
    public static <K, V> void printMap(Map<K, V> map) {
        for (Map.Entry<K, V> entry : map.entrySet()) {
            System.out.println("Key : " + entry.getKey() 
				+ " Value : " + entry.getValue());
        }
    }

    public Set<String> get_name_autocomplete(String str) {
        HashSet<String> res = (HashSet<String>) this.jedis.keys("Person:*:" + str + "*");

        HashMap<String, Integer> ret = new HashMap<String,Integer>();

        for (String person : res) {
            Integer count = Integer.parseInt(this.jedis.get(person));
            ret.put(person, count);
        }

        Map<String, Integer> sortedMap = new TreeMap<String, Integer>(new ValueComparator(ret));
        sortedMap.putAll(ret);

        // printMap(sortedMap);

        return sortedMap.keySet();
    }

    public void get_name_from_user() {
        Scanner in = new Scanner(System.in);

        while(true) {
            System.out.print("Search for ('Enter' for quit):");
            String input = in.nextLine();

            if(input.equals(""))
                break;

            Set<String> res = get_name_autocomplete(input);

            for(String name : res) 
                System.out.println(name.split(":")[2]);
        }

        in.close();
    }

    class ValueComparator implements Comparator<String> {
        Map<String, Integer> base;
    
        public ValueComparator(Map<String, Integer> base) {
            this.base = base;
        }
    
        // Note: this comparator imposes orderings that are inconsistent with
        // equals.
        public int compare(String a, String b) {
            if (base.get(a) >= base.get(b)) {
                return -1;
            } else {
                return 1;
            } // returning 0 would merge keys
        }
    }
}