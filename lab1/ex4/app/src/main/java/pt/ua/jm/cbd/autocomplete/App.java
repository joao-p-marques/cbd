package pt.ua.jm.cbd.autocomplete;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        // AutoComplete ac = new AutoComplete();
        // ac.insert_names();
        // ac.get_name_from_user();

        AutoComplete2 ac = new AutoComplete2();
        ac.insert_names();
        ac.get_name_from_user();
    }
}
