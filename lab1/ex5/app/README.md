## app Java Project

## Entidades

1. **Application**: Main class. Tem um *Message broker* e varios *Users*
2. **Message broker**: Entidade central que comunica com a BD e organiza os varios *Users*
3. **Users**: Utilizador do sistema de mensagens, que é caracterizado por um Id e por um nome.

## Execução

1. Conteúdo prévio da BD é limpo.
2. São criados 2 users.
3. O user 1 começa a seguir o user 2.
4. User 2 envia 2 mensagens para o sistema. User 1 envia 1 mensagem também.
5. User 1 consulta as mensagens para ele (dos que segue) e recebe as 2 mensagens enviadas por User 2.
6. (Como ninguém segue User 1, a sua mensagem não aparece em lado nenhum).