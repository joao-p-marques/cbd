
package pt.ua.jm.cbd.msg.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import redis.clients.jedis.Jedis;

class Messages {

    Jedis jedis;

    public Messages() {
        jedis = new Jedis("localhost");
    }

    public void add_user(User user) {
        this.jedis.set("Person:" + user.getId() + ":name", user.getName());
        System.out.println("User " + user.getName() + " with ID " + user.getId() + " added.");
    }

    public void user_follows(User user1, User user2) {
        this.jedis.sadd("Person:" + user2.getId() + ":followers", "Person:" + user1.getId());
        this.jedis.sadd("Person:" + user1.getId() + ":follows", "Person:" + user2.getId());
        System.out.println("User " + user1.getName() + " (" + user1.getId() + ") follows " + user2.getName() + " (" + user2.getId() + ").");
    }

    public void store_message(User user, String message) {
        this.jedis.lpush("Person:" + user.getId() + ":messages", message);
        System.out.println("User " + user.getName() + " (" + user.getId() + ") stored message: '" + message + "'");
    }

    public List<String> get_messages(User user) {
        Set<String> followed_by_user = this.jedis.smembers("Person:" + user.getId() + ":follows");
        List<String> msgs = new ArrayList<>();
        for(String u : followed_by_user) {
            msgs.addAll(this.jedis.lrange(u + ":messages", 0, -1));
        }
        return msgs;
        // return this.jedis.lrange("Person:" + user.getId() + ":messages", 0, -1);
    }

    public void clean(){
        this.jedis.flushDB();
        System.out.println("DB Cleaned.");
    }
}