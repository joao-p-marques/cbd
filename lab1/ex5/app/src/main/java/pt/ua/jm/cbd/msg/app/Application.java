package pt.ua.jm.cbd.msg.app;

public class Application {

    public static void main(String[] args) {
        System.out.println("Hello app");

        User user1 = new User("Joao");
        User user2 = new User("Pedro");

        Messages msg_broker = new Messages();
        msg_broker.clean();

        msg_broker.add_user(user1);
        msg_broker.add_user(user2);

        msg_broker.user_follows(user1, user2);

        msg_broker.store_message(user2, "ola");
        msg_broker.store_message(user2, "adeus");
        msg_broker.store_message(user1, "hello back");

        for (String msg : msg_broker.get_messages(user1)) {
            System.out.println(msg);
        }
    }

}