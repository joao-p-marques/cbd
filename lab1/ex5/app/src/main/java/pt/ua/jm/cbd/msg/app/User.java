
package pt.ua.jm.cbd.msg.app;

class User {
    int id;
    static int idC = 0;
    String name;

    public User(String name) {
        this.id = idC;
        idC++;
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public int getId() {
        return this.id;
    }
}