
package pt.ua.jm.cbd.jedis1;

import java.util.List;
import java.util.Set;
import redis.clients.jedis.Jedis;

public class SimplePostList {
    private Jedis jedis;

    public static String USERS = "users"; // Key set for users' name

    public SimplePostList() {
        this.jedis = new Jedis("localhost");
    }

    public void saveUser(String username) {
        jedis.lpush(USERS, username);
    }

    public List<String> getUser() {
        return jedis.lrange(USERS, 0, -1);
    }

    public Set<String> getAllKeys() {
        return jedis.keys("*");
    }

}