
package pt.ua.jm.cbd.jedis1;

import java.util.Set;
import redis.clients.jedis.Jedis;

public class SimplePost {
    private Jedis jedis;

    public static String USERS = "users"; // Key set for users' name

    public SimplePost() {
        this.jedis = new Jedis("localhost");
    }

    public void saveUser(String username) {
        jedis.sadd(USERS, username);
    }

    public Set<String> getUser() {
        return jedis.smembers(USERS);
    }

    public Set<String> getAllKeys() {
        return jedis.keys("*");
    }

}