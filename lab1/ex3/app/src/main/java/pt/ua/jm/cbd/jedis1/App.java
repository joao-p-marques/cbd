package pt.ua.jm.cbd.jedis1;

import java.util.HashMap;
import java.util.Map;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        // new Forum();

        // SimplePost board = new SimplePost();
        // SimplePostList board = new SimplePostList();
        SimplePostHashMap board = new SimplePostHashMap();

        // set some users
        String[] users = { "Ana", "Pedro", "Maria", "Luis" };

        Integer i = 0;
        for (String user: users) {
            board.saveUser(user, i);
            i++;
        }

        board.getAllKeys().stream().forEach(System.out::println);
        Map<String, String> result = board.getUser();
        for(String key : result.keySet())
            System.out.println(key + " -> " + result.get(key));
    }
}
