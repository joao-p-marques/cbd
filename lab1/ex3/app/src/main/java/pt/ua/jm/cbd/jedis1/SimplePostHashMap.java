
package pt.ua.jm.cbd.jedis1;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import redis.clients.jedis.Jedis;

public class SimplePostHashMap {
    private Jedis jedis;

    public static String USERS = "users:map"; // Key set for users' name

    public SimplePostHashMap() {
        this.jedis = new Jedis("localhost");
    }

    public void saveUser(String username, int id) {
        jedis.hset(USERS, username, Integer.toString(id));
    }

    public Map<String, String> getUser() {
        return jedis.hgetAll(USERS);
    }

    public Set<String> getAllKeys() {
        return jedis.keys("*");
    }

}